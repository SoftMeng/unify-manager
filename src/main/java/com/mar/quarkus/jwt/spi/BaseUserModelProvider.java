package com.mar.quarkus.jwt.spi;

import cn.hutool.crypto.SecureUtil;
import com.mar.quarkus.jwt.model.UserModel;

import java.util.Objects;

/**
 * Description: User Provider SPI .<br>
 *
 * @author m-xy
 *     Created By 2020/4/23 11:12
 */
public abstract class BaseUserModelProvider {
    /**
     * 用户信息
     *
     * @param userName 用户名
     * @return
     */
    public abstract UserModel getUserByUserName(String userName);

    /**
     * 密码校验
     * <p>
     * 123456: 8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92
     *
     * @param user
     * @param password
     * @return
     */
    public boolean checkPassword(UserModel user, String password) {
        if (Objects.isNull(password)) {
            return false;
        }
        return SecureUtil.sha256(password).equals(user.getPassword());
    }
}
