package com.mar.quarkus.jwt.model;

/**
 * Description: 登录 .<br>
 *
 * @author m-xy
 *     Created By 2020/4/23 11:56
 */
public class LoginModel {
    private String grantType;
    private String userName;
    private String password;

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
