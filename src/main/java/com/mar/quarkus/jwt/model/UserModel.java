package com.mar.quarkus.jwt.model;

import java.util.Set;

/**
 * Description: $VAL$ .<br>
 *
 * @author m-xy
 *     Created By 2020/4/23 11:11
 */
public class UserModel {
    private String userId;
    private String userName;
    private String password;
    private Set<String> roles;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
