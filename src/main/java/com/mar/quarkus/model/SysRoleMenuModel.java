package com.mar.quarkus.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 此类由角马科技自动生成工具生成.
 *
 * @author jm
 * @version 3.5
 */
@Entity
@Table(name = "sys_role_menu")
@Schema(description = "角色和菜单关联表")
public class SysRoleMenuModel extends BasePanacheEntity {

    @Schema(description = "角色ID")
    @Column(name = "role_id")
    public Long roleId;

    @Schema(description = "菜单ID")
    @Column(name = "menu_id")
    public Long menuId;

}
