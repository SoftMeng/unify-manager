package com.mar.quarkus.model;

import com.mar.quarkus.filter.JpaEntityListeners;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 此类由角马科技自动生成工具生成.
 *
 * @author jm
 * @version 3.5
 */
@Entity
@Table(name = "sys_user")
@Schema(description = "用户信息表")
@EntityListeners(JpaEntityListeners.class)
public class SysUserModel extends BasePanacheEntity {

    @Schema(description = "部门ID")
    @Column(name = "dept_id")
    public Long deptId;

    @Schema(description = "用户账号")
    @Column(name = "user_name")
    public String userName;

    @Schema(description = "用户昵称")
    @Column(name = "nick_name")
    public String nickName;

    @Schema(description = "用户类型（00系统用户）")
    @Column(name = "user_type")
    public String userType;

    @Schema(description = "用户邮箱")
    @Column(name = "email")
    public String email;

    @Schema(description = "手机号码")
    @Column(name = "phonenumber")
    public String phonenumber;

    @Schema(description = "用户性别（0男 1女 2未知）")
    @Column(name = "sex")
    public String sex;

    @Schema(description = "头像地址")
    @Column(name = "avatar")
    public String avatar;

    @Schema(description = "密码")
    @Column(name = "password")
    public String password;

    @Schema(description = "帐号状态（0正常 1停用）")
    @Column(name = "status")
    public String status;

    @Schema(description = "删除标志（0代表存续 2代表删除）")
    @Column(name = "del_flag")
    public String delFlag;

    @Schema(description = "最后登陆IP")
    @Column(name = "login_ip")
    public String loginIp;

    @Schema(description = "最后登陆时间")
    @Column(name = "login_date")
    public LocalDateTime loginDate;

    @Schema(description = "创建者")
    @Column(name = "create_by")
    public String createBy;

    @Schema(description = "创建时间")
    @Column(name = "create_time")
    public LocalDateTime createTime;

    @Schema(description = "更新者")
    @Column(name = "update_by")
    public String updateBy;

    @Schema(description = "更新时间")
    @Column(name = "update_time")
    public LocalDateTime updateTime;

    @Schema(description = "备注")
    @Column(name = "remark")
    public String remark;

    @Schema(description = "角色ID列表")
    @Transient
    public List<Long> roleIds;
}
