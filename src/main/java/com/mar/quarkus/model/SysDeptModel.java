package com.mar.quarkus.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 此类由角马科技自动生成工具生成.
 *
 * @author jm
 * @version 3.5
 */
@Entity
@Table(name = "sys_dept")
@Schema(description = "部门表")
public class SysDeptModel extends BasePanacheEntity {

    @Schema(description = "父部门id")
    @Column(name = "parent_id")
    public Long parentId;

    @Schema(description = "祖级列表")
    @Column(name = "ancestors")
    public String ancestors;

    @Schema(description = "部门名称")
    @Column(name = "dept_name")
    public String deptName;

    @Schema(description = "显示顺序")
    @Column(name = "number_sort")
    public Long numberSort;

    @Schema(description = "负责人")
    @Column(name = "leader")
    public String leader;

    @Schema(description = "联系电话")
    @Column(name = "phone")
    public String phone;

    @Schema(description = "邮箱")
    @Column(name = "email")
    public String email;

    @Schema(description = "部门状态（0正常 1停用）")
    @Column(name = "status")
    public String status;

    @Schema(description = "删除标志（0代表存在 2代表删除）")
    @Column(name = "del_flag")
    public String delFlag;

    @Schema(description = "创建者")
    @Column(name = "create_by")
    public String createBy;

    @Schema(description = "创建时间")
    @Column(name = "create_time")
    public LocalDateTime createTime;

    @Schema(description = "更新者")
    @Column(name = "update_by")
    public String updateBy;

    @Schema(description = "更新时间")
    @Column(name = "update_time")
    public LocalDateTime updateTime;

}
