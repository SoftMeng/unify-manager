package com.mar.quarkus.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 此类由角马科技自动生成工具生成.
 *
 * @author jm
 * @version 3.5
 */
@Entity
@Table(name = "sys_role_dept")
@Schema(description = "角色和部门关联表")
public class SysRoleDeptModel extends BasePanacheEntity {

    @Schema(description = "部门ID")
    @Column(name = "dept_id")
    public Long deptId;

    @Schema(description = "角色ID")
    @Column(name = "role_id")
    public Long roleId;

}
