package com.mar.quarkus.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 此类由角马科技自动生成工具生成.
 *
 * @author jm
 * @version 3.5
 */
@Entity
@Table(name = "sys_role")
@Schema(description = "角色信息表")
public class SysRoleModel extends BasePanacheEntity {

    @Schema(description = "角色名称")
    @Column(name = "role_name")
    public String roleName;

    @Schema(description = "角色权限字符串")
    @Column(name = "role_key")
    public String roleKey;

    @Schema(description = "显示顺序")
    @Column(name = "number_sort")
    public Long numberSort;

    @Schema(description = "数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）")
    @Column(name = "data_scope")
    public String dataScope;

    @Schema(description = "角色状态（0正常 1停用）")
    @Column(name = "status")
    public String status;

    @Schema(description = "删除标志（0代表存续 2代表删除）")
    @Column(name = "del_flag")
    public String delFlag;

    @Schema(description = "创建者")
    @Column(name = "create_by")
    public String createBy;

    @Schema(description = "创建时间")
    @Column(name = "create_time")
    public LocalDateTime createTime;

    @Schema(description = "更新者")
    @Column(name = "update_by")
    public String updateBy;

    @Schema(description = "更新时间")
    @Column(name = "update_time")
    public LocalDateTime updateTime;

    @Schema(description = "备注")
    @Column(name = "remark")
    public String remark;

    @Schema(description = "菜单ID列表")
    @Transient
    public List<Long> menuIds;

    @Schema(description = "部门ID列表")
    @Transient
    public List<Long> deptIds;
}
