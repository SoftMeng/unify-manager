package com.mar.quarkus.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

/**
 * Description: 分页返回数据$ .<br>
 *
 * @author m-xy
 *     Created By 2020/4/27 15:34
 */
@Data
@AllArgsConstructor
@Schema(description = "分页数据")
public class PageResponse<T> {
    private long total;
    private List<T> content;

    public static <T> PageResponse<T> page(long total, List<T> content) {
        return new PageResponse<>(total, content);
    }
}
