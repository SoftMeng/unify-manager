package com.mar.quarkus.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 此类由角马科技自动生成工具生成.
 *
 * @author jm
 * @version 3.5
 */
@Entity
@Table(name = "sys_user_role")
@Schema(description = "用户和角色关联表")
public class SysUserRoleModel extends BasePanacheEntity {

    @Schema(description = "用户ID")
    @Column(name = "user_id")
    public Long userId;

    @Schema(description = "角色ID")
    @Column(name = "role_id")
    public Long roleId;

}
