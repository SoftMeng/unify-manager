package com.mar.quarkus.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 此类由角马科技自动生成工具生成.
 *
 * @author jm
 * @version 3.5
 */
@Entity
@Table(name = "sys_menu")
@Schema(description = "菜单权限表")
public class SysMenuModel extends BasePanacheEntity {

    @Schema(description = "菜单名称")
    @Column(name = "menu_name")
    public String menuName;

    @Schema(description = "父菜单ID")
    @Column(name = "parent_id")
    public Long parentId;

    @Schema(description = "显示顺序")
    @Column(name = "number_sort")
    public Long numberSort;

    @Schema(description = "路由地址")
    @Column(name = "path")
    public String path;

    @Schema(description = "组件路径")
    @Column(name = "component")
    public String component;

    @Schema(description = "是否为外链（0是 1否）")
    @Column(name = "is_frame")
    public Long isFrame;

    @Schema(description = "菜单类型（M目录 C菜单 F按钮）")
    @Column(name = "menu_type")
    public String menuType;

    @Schema(description = "菜单状态（0显示 1隐藏）")
    @Column(name = "visible")
    public String visible;

    @Schema(description = "权限标识")
    @Column(name = "perms")
    public String perms;

    @Schema(description = "菜单图标")
    @Column(name = "icon")
    public String icon;

    @Schema(description = "删除标志（0代表存续 2代表删除）")
    @Column(name = "del_flag")
    public String delFlag;

    @Schema(description = "创建者")
    @Column(name = "create_by")
    public String createBy;

    @Schema(description = "创建时间")
    @Column(name = "create_time")
    public LocalDateTime createTime;

    @Schema(description = "更新者")
    @Column(name = "update_by")
    public String updateBy;

    @Schema(description = "更新时间")
    @Column(name = "update_time")
    public LocalDateTime updateTime;

    @Schema(description = "备注")
    @Column(name = "remark")
    public String remark;

}
