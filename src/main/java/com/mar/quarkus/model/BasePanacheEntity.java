package com.mar.quarkus.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Description: 定义ORM继承,使用主键自增 .<br>
 *
 * @author m-xy
 *     Created By 2020/4/26 14:32
 */
@MappedSuperclass
public class BasePanacheEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public BasePanacheEntity() {
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "<" + this.id + ">";
    }
}
