package com.mar.quarkus.resource;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.mar.quarkus.constants.DelFlag;
import com.mar.quarkus.model.PageResponse;
import com.mar.quarkus.model.SysRoleDeptModel;
import com.mar.quarkus.model.SysRoleMenuModel;
import com.mar.quarkus.model.SysRoleModel;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

/**
 * Description: 系统角色管理 .<br>
 *
 * @author m-xy Created By 2020/4/28 10:20
 */
@Tag(name = "系统角色管理")
@Path("/sys/role")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SysRoleResource {

    @GET
    @Operation(summary = "查询记录列表")
    public PageResponse<SysRoleModel> list(
        @QueryParam("key") String key,
        @QueryParam("index") int index,
        @QueryParam("size") int size
    ) {
        StringBuilder sql = new StringBuilder("delFlag = :delFlag ");
        Parameters parameters = Parameters.with("delFlag", DelFlag.LIVE.getCode());
        if (Objects.nonNull(key)) {
            sql.append("and roleName like :roleName ");
            parameters.and("roleName", "%" + key + "%");
        }
        PanacheQuery<SysRoleModel> query = SysRoleModel.find(sql.toString(), parameters);
        query.page(Page.of(index, size == 0 ? 10 : size));
        List<SysRoleModel> content = query.list();
        long total = query.count();
        return PageResponse.page(total, content);
    }

    @GET
    @Path("/{id}")
    @Operation(summary = "查询记录")
    public SysRoleModel findOne(@PathParam("id") Long id) {
        SysRoleModel model = SysRoleModel.findById(id);
        return model;
    }

    @DELETE
    @Path("/{id}")
    @Operation(summary = "逻辑删除记录")
    @Transactional(rollbackOn = Exception.class)
    public Response delete(@PathParam("id") Long id) {
        Assert.notNull(id);
        SysRoleModel model = SysRoleModel.findById(id);
        Assert.notNull(model);
        model.delFlag = DelFlag.DELETED.getCode();
        return Response.ok().build();
    }

    @POST
    @Operation(summary = "新增记录")
    @Transactional(rollbackOn = Exception.class)
    public Response save(@RequestBody SysRoleModel sysRoleModel) {
        Assert.isNull(sysRoleModel.id);
        sysRoleModel.persist();
        saveRoleMenus(sysRoleModel);
        saveRoleDepts(sysRoleModel);
        return Response.ok().build();
    }

    /**
     * 保存角色菜单信息.
     *
     * @param sysUserModel
     */
    private void saveRoleMenus(SysRoleModel sysRoleModel) {
        if (CollectionUtil.isNotEmpty(sysRoleModel.menuIds)) {
            sysRoleModel.menuIds.forEach(menuId -> {
                SysRoleMenuModel sysRoleMenuModel = new SysRoleMenuModel();
                sysRoleMenuModel.roleId = sysRoleModel.id;
                sysRoleMenuModel.menuId = menuId;
                sysRoleMenuModel.persist();
            });
        }
    }

    /**
     * 保存角色部门信息.
     *
     * @param sysUserModel
     */
    private void saveRoleDepts(SysRoleModel sysRoleModel) {
        if (CollectionUtil.isNotEmpty(sysRoleModel.deptIds)) {
            sysRoleModel.deptIds.forEach(deptId -> {
                SysRoleDeptModel sysRoleDeptModel = new SysRoleDeptModel();
                sysRoleDeptModel.roleId = sysRoleModel.id;
                sysRoleDeptModel.deptId = deptId;
                sysRoleDeptModel.persist();
            });
        }
    }

    @PUT
    @Operation(summary = "修改记录")
    @Transactional(rollbackOn = Exception.class)
    public Response update(@RequestBody SysRoleModel sysRoleModel) {
        Assert.notNull(sysRoleModel.id);
        SysRoleModel model = SysRoleModel.findById(sysRoleModel.id);
        Assert.notNull(model);
        BeanUtil.copyProperties(sysRoleModel, model, "id");
        SysRoleMenuModel.delete("roleId", sysRoleModel.id);
        saveRoleMenus(sysRoleModel);
        SysRoleDeptModel.delete("roleId", sysRoleModel.id);
        saveRoleDepts(sysRoleModel);
        return Response.ok().build();
    }
}
