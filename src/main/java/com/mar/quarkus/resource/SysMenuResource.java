package com.mar.quarkus.resource;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import com.mar.quarkus.constants.DelFlag;
import com.mar.quarkus.model.PageResponse;
import com.mar.quarkus.model.SysMenuModel;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

/**
 * Description: 系统菜单管理 .<br>
 *
 * @author m-xy Created By 2020/4/28 10:13
 */
@Tag(name = "系统菜单管理")
@Path("/sys/menu")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SysMenuResource {

    @GET
    @Operation(summary = "查询记录列表")
    public PageResponse<SysMenuModel> list(
        @QueryParam("key") String key,
        @QueryParam("index") int index,
        @QueryParam("size") int size
    ) {
        StringBuilder sql = new StringBuilder("delFlag = :delFlag ");
        Parameters parameters = Parameters.with("delFlag", DelFlag.LIVE.getCode());
        if (Objects.nonNull(key)) {
            sql.append("and menuName like :menuName ");
            parameters.and("menuName", "%" + key + "%");
        }
        PanacheQuery<SysMenuModel> query = SysMenuModel.find(sql.toString(), parameters);
        query.page(Page.of(index, size == 0 ? 10 : size));
        List<SysMenuModel> content = query.list();
        long total = query.count();
        return PageResponse.page(total, content);
    }

    @GET
    @Path("/{id}")
    @Operation(summary = "查询记录")
    public SysMenuModel findOne(@PathParam("id") Long id) {
        SysMenuModel model = SysMenuModel.findById(id);
        return model;
    }

    @DELETE
    @Path("/{id}")
    @Operation(summary = "逻辑删除记录")
    @Transactional(rollbackOn = Exception.class)
    public Response delete(@PathParam("id") Long id) {
        Assert.notNull(id);
        SysMenuModel model = SysMenuModel.findById(id);
        Assert.notNull(model);
        model.delFlag = DelFlag.DELETED.getCode();
        return Response.ok().build();
    }

    @POST
    @Operation(summary = "新增记录")
    @Transactional(rollbackOn = Exception.class)
    public Response save(@RequestBody SysMenuModel sysMenuModel) {
        Assert.isNull(sysMenuModel.id);
        sysMenuModel.persist();
        return Response.ok().build();
    }

    @PUT
    @Operation(summary = "修改记录")
    @Transactional(rollbackOn = Exception.class)
    public Response update(@RequestBody SysMenuModel sysMenuModel) {
        Assert.notNull(sysMenuModel.id);
        SysMenuModel model = SysMenuModel.findById(sysMenuModel.id);
        Assert.notNull(model);
        BeanUtil.copyProperties(sysMenuModel, model, "id");
        return Response.ok().build();
    }
}
