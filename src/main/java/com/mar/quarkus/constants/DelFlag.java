package com.mar.quarkus.constants;

/**
 * Description: 删除标记$ .<br>
 *
 * @author m-xy
 *     Created By 2020/4/28 09:56
 */
public enum DelFlag {
    /**
     * .
     */
    LIVE("0", "存续"),
    DELETED("2", "已删除");

    private final String code;

    private final String name;

    DelFlag(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 通过Code获取Name .
     *
     * @param code 编码.
     * @return 名称.
     */
    public static String name(String code) {
        return java.util.Arrays.stream(values())
            .filter(e -> e.getCode().equals(code))
            .findFirst()
            .map(DelFlag::getName).orElse("");
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
