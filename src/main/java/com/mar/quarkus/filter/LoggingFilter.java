package com.mar.quarkus.filter;

import io.vertx.core.http.HttpServerRequest;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

/**
 * Description: 日志拦截器 .<br>
 *
 * @author m-xy
 *     Created By 2020/4/27 14:49
 */
@Provider
public class LoggingFilter implements ContainerRequestFilter {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(LoggingFilter.class);
    @Context UriInfo info;
    @Context HttpServerRequest request;

    @Override
    public void filter(ContainerRequestContext context) {
        final String method = context.getMethod();
        final String path = info.getPath();
        final String address = request.remoteAddress().toString();
        LOGGER.info("{}:{}, From {}", method, path, address);
    }
}

