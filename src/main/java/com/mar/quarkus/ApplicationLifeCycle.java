package com.mar.quarkus;

import io.quarkus.runtime.StartupEvent;
import io.quarkus.runtime.configuration.ProfileManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

/**
 * Description: 生命周期 .<br>
 *
 * @author 孟祥元
 * @Date 2020/6/5 11:05
 */
@ApplicationScoped
public class ApplicationLifeCycle {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationLifeCycle.class);

    void onStart(@Observes StartupEvent ev) {
        // 你说我们敲代码有什么用呀？用户也看不见……
        LOGGER.info("The application is starting with profile : {}, launch mode: {}",
            ProfileManager.getActiveProfile(), ProfileManager.getLaunchMode());
    }
}
