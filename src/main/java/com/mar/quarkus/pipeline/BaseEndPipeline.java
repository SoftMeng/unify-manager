package com.mar.quarkus.pipeline;

/**
 * Description: 终点管道 .<br>
 *
 * @author 孟祥元
 * @Date 2020/9/15 18:21
 */
public abstract class BaseEndPipeline<T> implements Pipeline<T> {
    private final String name;

    public BaseEndPipeline(String name) {
        this.name = name;
    }

    /**
     * 终点流程.
     *
     * @param ctx
     * @param t
     */
    @Override
    public abstract void process(PipelineContext ctx, T t);

    @Override
    public final void forward(PipelineContext ctx, T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return name;
    }
}
