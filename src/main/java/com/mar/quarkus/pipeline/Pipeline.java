package com.mar.quarkus.pipeline;

/**
 * Description: 管道接口 .<br>
 *
 * @author 孟祥元
 * @Date 2020/9/15 18:17
 */
public interface Pipeline<T> {

    /**
     * 当前的处理流程.
     *
     * @param ctx
     * @param t
     */
    void process(PipelineContext ctx, T t);

    /**
     * 转发给下游的流程.
     *
     * @param ctx
     * @param t
     */
    void forward(PipelineContext ctx, T t);
}
