package com.mar.quarkus.pipeline;

import lombok.NonNull;

/**
 * Description: 中间管道 .<br>
 *
 * @author 孟祥元
 * @Date 2020/9/15 18:19
 */
public abstract class BaseMiddlePipeline<T> implements Pipeline<T> {

    private final String name;

    private final Pipeline<? super T> next;

    public BaseMiddlePipeline(@NonNull String name, @NonNull Pipeline<? super T> next) {
        this.name = name;
        this.next = next;
    }

    /**
     * 当前流程.
     *
     * @param ctx
     * @param t
     */
    @Override
    public abstract void process(PipelineContext ctx, T t);

    @Override
    public void forward(PipelineContext ctx, T t) {
        next.process(ctx, t);
    }

    @Override
    public String toString() {
        return name + " -> " + next.toString();
    }
}
