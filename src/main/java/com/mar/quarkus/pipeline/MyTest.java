package com.mar.quarkus.pipeline;

/**
 * Description: 测试 .<br>
 *
 * @author 孟祥元
 * @Date 2020/9/15 18:28
 */
public class MyTest {

    public static void main(String[] args) {
        Pipeline<String> pipeline =
            new FunctionalPipelineBuilder<String, Pipeline<String>>()
                .add(next -> new MyMiddlePipeline("1", next))
                .add(next -> new MyMiddlePipeline("2", next))
                .end(new MyEndPipeline("3"));
        System.err.println(pipeline.toString());
        pipeline.process(new PipelineContext(), "0");
    }
}
