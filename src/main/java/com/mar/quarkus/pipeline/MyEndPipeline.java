package com.mar.quarkus.pipeline;

import lombok.NonNull;

/**
 * Description: 我的中间管道 .<br>
 *
 * @author 孟祥元
 * @Date 2020/9/15 18:23
 */
public final class MyEndPipeline extends BaseEndPipeline<String> {
    public MyEndPipeline(@NonNull String name) {
        super(name);
    }

    @Override
    public void process(PipelineContext ctx, String s) {
        System.err.println("完了？".concat(s));
    }
}
