package com.mar.quarkus.pipeline;

import lombok.NonNull;

/**
 * Description: 我的中间管道 .<br>
 *
 * @author 孟祥元
 * @Date 2020/9/15 18:23
 */
public final class MyMiddlePipeline extends BaseMiddlePipeline<String> {
    public MyMiddlePipeline(@NonNull String name, @NonNull Pipeline<? super String> next) {
        super(name, next);
    }

    @Override
    public void process(PipelineContext ctx, String s) {
        System.err.println("干点啥呢？".concat(s));
        s = "1111";
        super.forward(ctx, s);
    }
}
