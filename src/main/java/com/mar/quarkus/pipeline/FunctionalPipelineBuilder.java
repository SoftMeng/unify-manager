package com.mar.quarkus.pipeline;

import java.util.function.Function;

/**
 * Description: 管道函数 .<br>
 *
 * @author 孟祥元
 * @Date 2020/9/15 18:18
 */
@SuppressWarnings("unchecked")
public final class FunctionalPipelineBuilder<T, P extends Pipeline<T>> {
    private final Function<Pipeline<? super T>, P> factory;

    public FunctionalPipelineBuilder() {
        this((Pipeline<? super T> next) -> (P) next);
    }

    private FunctionalPipelineBuilder(Function<Pipeline<? super T>, P> nextFactory) {
        this.factory = nextFactory;
    }

    public FunctionalPipelineBuilder<T, P> add(
        Function<Pipeline<? super T>, ? extends BaseMiddlePipeline<T>> nextFactory) {
        return new FunctionalPipelineBuilder<>(factory.compose(nextFactory));
    }

    public P end(BaseEndPipeline<T> baseEndPipeline) {
        return factory.apply(baseEndPipeline);
    }
}
