# unify-manager project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Test

```js
Running 1m test @ http://127.0.0.1:8080/hello
  2 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.47ms    0.87ms  45.99ms   98.12%
    Req/Sec    33.26k     1.89k   43.58k    90.58%
  Latency Distribution
     50%    1.41ms
     75%    1.50ms
     90%    1.65ms
     99%    2.70ms
  3972928 requests in 1.00m, 314.48MB read
Requests/sec:  66150.00
Transfer/sec:      5.24MB
```

## Packaging and running the application

The application can be packaged using `./mvnw package`.
It produces the `unify-manager-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/unify-manager-1.0.0-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: `./mvnw package -Pnative`.

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: `./mvnw package -Pnative -Dquarkus.native.container-build=true`.

You can then execute your native executable with: `./target/unify-manager-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/building-native-image.

